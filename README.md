# About

This package contains utility functions and scripts extending the signac framework.

This package allows us to extend the framework easily with new functions without the need to necessarily extend the core package right away.
Functions that prove to be useful with a fully fletched out API might eventually become part of the core package.

*Maintainer: Carl Simon Adorf <csadorf@umich.edu>*

# Installation

To install this package, excute

```
#!bash
$ python setup.py install
```

or with **conda**
```
#!bash
conda install -c glotzer signac-utils
```

# Extensions

This is a brief overview of extensions that are part of this package:
 
## signac-rm

Allows you to remove a job as part of a signac project.

For example, to remove a job with an id that starts with `abdf4`, execute
```
#!bash
$ signac-rm abdf4
```
To remove **all** jobs where a the state point parameter `a` is equal to 4, you could execute
```
#!bash
$ signac find '{"a": 4}' | xargs signac-rm
```

## signac-sync

Synchronize signac projects across different endpoints using rsync.

Execute `signac-sync --help` for more information.