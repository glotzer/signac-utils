# Copyright (c) 2017 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the BSD 3-Clause License.
import sys

from setuptools import setup, find_packages

setup(
    name='signac-utils',
    version='0.2.2',
    author='Carl Simon Adorf',
    author_email='csadorf@umich.edu',
    description="Utility package for the signac framework.",

    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
    ],

    scripts = [
        'bin/signac-rm',
        'bin/signac-sync',
        'bin/flow-clone',
        'bin/flow-test',
        'bin/detect-schema',
        'bin/signac-benchmark',
        ],
)
