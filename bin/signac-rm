#!/usr/bin/env python
# Copyright (c) 2017 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the BSD 3-Clause License.
import sys
import argparse
import logging

import signac

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument(
    'job_id',
    type=str,
    nargs='+',
    help="Specify the job ids to remove.")
args = parser.parse_args()

project = signac.get_project()

for job_id in args.job_id:
    try:
        project.open_job(id=job_id).remove()
    except KeyError:
        print("No job found for id '{}'.".format(job_id), file=sys.stderr)
    except LookupError:
        print("Found multiple jobs matching id '{}'.".format(job_id), file=sys.stderr)
    else:
        print("Removed job with id '{}'.".format(job_id))
