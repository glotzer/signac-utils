#!/usr/bin/env python
# Copyright (c) 2017 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the BSD 3-Clause License.
import os
import argparse
import logging
import timeit
import random

import signac
from tqdm import tqdm


logger = logging.getLogger(__name__)


class Timer(timeit.Timer):

    def __init__(self, stmt, setup=None, descr=None, **kwargs):
        if setup is None:
            setup = 'import signac; project = signac.get_project()'
        self.descr = stmt if descr is None else descr
        super().__init__(stmt=stmt, setup=setup, **kwargs)

    def timeit(self, number=10):
        return number, super().timeit(number=number)


def size(fn):
    try:
        return os.stat(fn).st_size
    except FileNotFoundError:
        return 0


def calc_size(project):
    sp_size = []
    doc_size = []
    for job in tqdm(project, 'determine metadata size'):
        sp_size.append(size(job.fn(job.FN_MANIFEST)))
        doc_size.append(size(job.fn(job.FN_DOCUMENT)))
    return sp_size, doc_size


def fmt_size(size, units=None):
    "Returns a human readable string reprentation of bytes."
    if units is None:
        units = [' bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB']
    return str(size) + units.pop(0) if size < 1024 else fmt_size(size >> 10, units[1:])


def total(benchmarks):
    n = sum((b[0] for b in benchmarks))
    dt = sum((b[1] for b in benchmarks))
    return n, dt


def print_result(n, dt):
    print("Mean time: {:.2}s ({} iterations)".format(dt / n, n))


def main(args):
    project = signac.get_project()

    print("Total # of jobs:", len(project))

    sp_size, doc_size = calc_size(project)
    print("Statepoint metadata: ", fmt_size(sum(sp_size)))
    print("Document metadata:   ", fmt_size(sum(doc_size)))
    print("Total metadata:      ", fmt_size(sum(sp_size) + sum(doc_size)))

    print("Determine project size:")
    t = Timer('len(project)')
    print_result(*t.timeit(100))

    print("Iterate through data space:")
    t = Timer('for job in project: pass')
    print_result(*t.timeit(10))

    print("Iterate through all job documents:")
    t = Timer('for job in project: job.document')
    print_result(*t.timeit(10))
    t.timeit()

    print("Generate the project index:")
    t = Timer('list(project.index())')
    print_result(*t.timeit())
    t.timeit()

    print("Open jobs by id:")
    b = []
    for job in random.sample(list(project), 10):
        t = Timer('project.open_job(id={})'.format(repr(job.get_id())))
        b.append(t.timeit())
    print_result(*total(b))

    print("Find jobs by statepoint:")
    b = []
    for job in random.sample(list(project), 3):
        t = Timer('project.find_jobs({})'.format(repr(job.statepoint())))
        b.append(t.timeit())
    print_result(*total(b))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Run benchmarks on project.")
    args = parser.parse_args()
    logging.basicConfig(level=logging.WARNING)
    main(args)
