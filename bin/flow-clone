#!/usr/bin/env python
# Copyright (c) 2017 The Regents of the University of Michigan
# All rights reserved.
# This software is licensed under the BSD 3-Clause License.
import os
import sys
import shutil
import logging
import re
import argparse
from collections import defaultdict as ddict
from shutil import copyfileobj
from urllib.parse import urlparse
from getpass import getuser, getpass
from contextlib import contextmanager
from tempfile import TemporaryDirectory
import zipfile
import tarfile
from io import BytesIO

import signac
try:
    from tqdm import tqdm
except ImportError:
    class tqdm:
        "Mocking the tqdm class, when tqdm is not available."

        def __init__(self, desc=None, total=None, file=None):
            assert total >= 0
            self.desc = desc
            self.total = total
            self.file = file
            self._counter = 0

        def __enter__(self):
            return self

        def __exit__(self, exc_val, exc_type, tb):
            return

        def update(self, i=1):
            self._counter += i
            print(
                '{}: {}/{}'.format(self.desc, self._counter+1, self.total),
                end='\r', file=self.file)


EXCLUDE_PATTERN_AS_TEMPLATE = '.*/(\.git/|\.pyc$|__pycache__|signac.rc$)'

GIT_WARNING_MSG = """The prototype path appears to be a git repository.
The recommended method for cloning git repositories is:

    git clone {src} {dst}

Use the '--ignore-git' argument to ignore this warning and copy anyways."""

DST_IS_GIT_WARNING_MSG = """The destination path '{dst}' exists and is
a git repository. Use the '--exclude-git' argument to copy anyways."""

SIGNAC_PROJECT_WARNING_MSG = """The destination path '{dst}' already contains a signac
project configuration.

Use the '--ignore-existing' argument to ignore this warning and copy anyways."""


def is_git_url(url):
    "Test wether given url might be a git-url."
    # Regular expression adapted from:

    #    is-git-url <https://github.com/jonschlinkert/is-git-url>
    #
    #    Copyright (c) 2014-2015, 2017, Jon Schlinkert.
    #    Released under the MIT License.

    #    The MIT License (MIT)

    #    Copyright (c) 2014-2015, 2017, Jon Schlinkert

    #    Permission is hereby granted, free of charge, to any person obtaining a copy
    #    of this software and associated documentation files (the "Software"), to deal
    #    in the Software without restriction, including without limitation the rights
    #    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    #    copies of the Software, and to permit persons to whom the Software is
    #    furnished to do so, subject to the following conditions:

    #    The above copyright notice and this permission notice shall be included in
    #    all copies or substantial portions of the Software.

    #    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    #    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    #    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    #    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    #    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    #    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    #    THE SOFTWARE.
    GIT_URL_PATTERN = '(?:git|ssh|https?|git@[-\w.]+):(\/\/)?(.*?)(\.git)(\/?|\#[-\d\w._]+?)$'
    return re.match(GIT_URL_PATTERN, url) is not None


class AuthenticationError(RuntimeError):

    def __str__(self):
        return "Authentication failed for repository with url '{}'.".format(self.args[0])


class FetchError(RuntimeError):

    def __str__(self):
        return "Unable to fetch from repository with url '{}', status code: {}.".format(
            self.args[0], self.args[1])


def _https_fetch(session, url, auth=None, **kwargs):
    r = session.get(url, auth=auth, **kwargs)
    if r.status_code == 401:
        if auth is None:
            u = urlparse(url)
            if u.username is None:
                username = getuser()
            else:
                username = u.username
            if u.password is None:
                password = getpass("Password ({}): ".format(username))
            else:
                password = u.password
            return _https_fetch(session, url, auth=(username, password), **kwargs)
        else:
            raise AuthenticationError(url)
    elif r.status_code == 200:
        return r
    else:
        raise FetchError(url, r.status_code)


def _root_or_single_folder(path):
    if len(os.listdir(path)) == 1:
        return os.path.join(path, os.listdir(path)[0])
    else:
        return path


@contextmanager
def fetch_prototype_lib(url):
    logging.debug("Fetching library from url '{}'...".format(url))
    if url is None:
        yield '.'
    else:
        u = urlparse(url)
        if u.scheme == 'git' or is_git_url(url):
            logging.debug("Fetching from remote git repository.")
            from git import Repo
            with TemporaryDirectory() as tmp:
                Repo.clone_from(url, tmp)
                yield tmp
        elif u.scheme in ('http', 'https'):
            try:
                import requests
            except ImportError:
                raise RuntimeError("Downloads require the 'requests' package.")
            else:
                ext = os.path.splitext(u.path)[1]
                if ext in ('.zip', '.tar', '.gz'):
                    with TemporaryDirectory() as tmp:
                        session = requests.Session()
                        r = _https_fetch(session, url, stream=True)
                        if ext == '.zip':
                            z = zipfile.ZipFile(BytesIO(r.content))
                            z.extractall(tmp)
                            yield _root_or_single_folder(tmp)
                        elif ext in ('.tar', '.gz'):
                            mode = 'r:gz' if ext == '.gz' else 'r'
                            with tarfile.open(mode=mode, fileobj=BytesIO(r.content)) as t:
                                t.extractall(tmp)
                                yield _root_or_single_folder(tmp)
                        else:
                            raise NotImplementedError(
                                "Can't open archive with extension '{}'.".format(ext))
                else:
                    raise ValueError("Unknown file extension '{}'.".format(ext))
        else:
            logging.debug("Fetching from local file path.")
            yield os.path.abspath(os.path.expanduser(url))


def _is_git_root_directory(path):
    return os.path.exists(os.path.join(path, '.git'))


def _is_signac_root_directory(path):
    try:
        signac.get_project(root=path)
    except LookupError:
        return False
    else:
        return True


def _find_workspaces(path):
    "Find all workspaces within the given path."
    workspaces = ddict(set)
    for dirpath, dirnames, _ in os.walk(path):
        if dirpath in workspaces:
            # Don't look for projects within workspaces.
            dirnames.clear()
            continue
        else:
            try:
                project = signac.get_project(root=dirpath)
                workspaces[os.path.realpath(project.workspace())].add(os.path.realpath(dirpath))
            except LookupError:
                pass
    return workspaces


def _copy_file(fn_src, fn_dst):
    "Copy file from fn_src to fn_dst."
    with open(fn_src, 'rb') as src:
        with open(fn_dst, 'xb') as dst:
            copyfileobj(src, dst)
    shutil.copymode(fn_src, fn_dst)


class Prototype:

    def __init__(self, path):
        if os.path.isdir(path):
            self._path = path
        else:
            raise FileNotFoundError(path)

    @property
    def path(self):
        return self._path

    @property
    def name(self):
        return os.path.basename(self.path)

    def get_id(self):
        return signac.get_project(root=self.path).get_id()

    def __str__(self):
        return self.name


def clone_prototype(prototype, directory, skip_workspaces=True, exclude=None, log=None):
    """Copy the prototype directory into directory.

    :param prototype: The prototype to copy
    :type prototype: Prototype
    :param directory: The destination path to copy the prototype into.
    :type directory: str
    :param skip_workspaces: Skip all directories that are configured as workspace for
        a project that is part of the prototype.
    :type skip_workspaces: bool
    :param exclude: All files with relative paths that match this regular expression
        will be excluded from copying.
    :type exclude: str
    :param log: A log-file to write messages to.
    :type log: file-like object
    """
    if log is None:
        log = sys.stderr

    if skip_workspaces:
        print("Determine project and workspace directories...", file=log)
        workspaces = _find_workspaces(prototype.path)
        if workspaces:
            print("Workspaces found:", file=log)
            for ws in sorted(workspaces):
                print(' -', ws, file=log)
    else:
        workspaces = None

    # Collect all copy paths...
    print("Determine files to copy...", file=log)
    transactions = ddict(list)
    for dirpath, dirnames, filenames in os.walk(prototype.path):
        # Skip all workspace paths if required.
        if workspaces and os.path.realpath(dirpath) in workspaces:
            dirnames.clear()
            continue

        rel_dst_dir = os.path.relpath(dirpath, prototype.path)
        for fn in filenames:
            if exclude:
                fn_ = os.path.join(rel_dst_dir, fn)
                if re.match(exclude, fn_):
                    logging.info("Skipping '{}', excluded.".format(fn_))
                    continue
            transactions[rel_dst_dir].append(fn)

    total = sum(len(_) for _ in transactions.values())
    with tqdm(total=total, desc='Copy files', file=log) as pbar:
        for dirpath in sorted(transactions):
            os.makedirs(os.path.join(directory, dirpath), exist_ok=True)
            for fn in sorted(transactions[dirpath]):
                fn_src = os.path.normpath(os.path.join(prototype.path, dirpath, fn))
                fn_dst = os.path.normpath(os.path.join(directory, dirpath, fn))
                try:
                    _copy_file(fn_src, fn_dst)
                except FileNotFoundError:
                    logging.warning("Failed to copy file '{}'.".format(fn_src))
                except FileExistsError:
                    logging.warning("File '{}' already exists.".format(fn_dst))
                else:
                    logging.info("Copied file '{}'.".format(fn_dst))
                finally:
                    pbar.update()


def main(arg):
    log = open(os.devnull, 'w') if args.quiet else sys.stderr

    num_hash = args.prototype.count('#')
    if num_hash == 0:
        remote, path = None, args.prototype
    elif num_hash == 1:
        remote, path = args.prototype.split('#')
    else:
        raise RuntimeWarning("Ill-formed path '{}'.".format(args.prototype))

    with fetch_prototype_lib(remote) as lib:
        try:
            prototype = Prototype(os.path.join(lib, path))
        except FileNotFoundError:
            raise RuntimeWarning("The source path '{}' does not exist.".format(path))

        if args.id is None:
            try:
                args.id = signac.get_project(root=prototype.path).get_id()
            except LookupError:
                args.id = prototype.name

        if args.directory is None:
            args.directory = prototype.name

        if _is_git_root_directory(prototype.path):
            if args.ignore_git:
                logging.warning(
                    "The prototype path '{}' is a git repository.".format(prototype.path))
            else:
                raise RuntimeWarning(GIT_WARNING_MSG.format(src=prototype.path, dst=prototype.name))

        if _is_git_root_directory(args.directory):
            if args.exclude_git:
                logging.warning(
                    "The destination path '{}' exists and "
                    "is a git repository.".format(args.directory))
            else:
                raise RuntimeWarning(DST_IS_GIT_WARNING_MSG.format(dst=args.directory))

        if _is_signac_root_directory(args.directory):
            if args.ignore_existing:
                logging.warning(
                    "The destination path '{}' contains a signac project.".format(args.directory))
            else:
                raise RuntimeWarning(SIGNAC_PROJECT_WARNING_MSG.format(dst=args.directory))

        clone_prototype(
            prototype=prototype,
            directory=args.directory,
            skip_workspaces=not args.include_workspaces,
            exclude=args.exclude,
            log=log)
        try:
            signac.init_project(root=args.directory, name=args.id)
        except RuntimeError:
            logging.warning(
                "Not initializing project, path '{}', already contains a "
                "conflicting project configuration.".format(args.directory))
        else:
            print("Initialized signac project '{}' in '{}'.".format(
                args.id, args.directory, file=log))


class VerbosityAction(argparse.Action):

    def __call__(self, parser, args, values, option_string=None):
        if values is None:
            values = '1'
        try:
            values = int(values)
        except ValueError:
            values = values.count('v') + 1
        setattr(args, self.dest, values)


class VerbosityLoggingConfigAction(VerbosityAction):

    def __call__(self, parser, args, values, option_string=None):
        super(VerbosityLoggingConfigAction, self).__call__(
            parser, args, values, option_string)
        v_level = getattr(args, self.dest)
        quiet = getattr(args, 'quiet', False)
        if quiet:
            logging.basicConfig(level=logging.ERROR)
        else:
            logging.basicConfig(level=logging.ERROR - 10 * v_level)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Clone an existing signac project as "
                    "prototype for a new project.")
    parser.add_argument(
        'prototype',
        help="The path of the prototype project to clone.")
    parser.add_argument(
        'directory',
        nargs='?',
        help="The path to clone the prototype project into. "
             "Defaults to basename of the prototype path.")
    parser.add_argument(
        '--id',
        help="Explicitly specify the id of the new project.")
    parser.add_argument(
        '--include-workspaces',
        action='store_true',
        help="Do not skip the workspaces that are associated within any "
             "project within the prototype path.")
    parser.add_argument(
        '--exclude',
        help="All files that match the given regular expression pattern "
             "will be excluded from copying.")
    parser.add_argument(
        '--exclude-git',
        action='store_true',
        help="Do not copy the git repository metadata.")
    parser.add_argument(
        '--ignore-git',
        action='store_true',
        help="Copy, even if the prototype path is a git repository.")
    parser.add_argument(
        '--ignore-existing',
        action='store_true',
        help="Copy, even if the target directory already contains a signac "
             "project configuration.")
    parser.add_argument(
        '--as-template',
        action='store_true',
        help="Copy the prototype as template. "
             "Implies --exclude='{}'.".format(EXCLUDE_PATTERN_AS_TEMPLATE))
    parser.add_argument(
        '-q', '--quiet',
        action='store_true',
        help="Suppress most output.")
    parser.add_argument(
        '-v',
        default=0,
        nargs='?',
        action=VerbosityLoggingConfigAction,
        dest='verbosity',
        help="Increase output verbosity.")
    args = parser.parse_args()

    if args.as_template:
        if args.exclude is None:
            args.exclude = EXCLUDE_PATTERN_AS_TEMPLATE
        else:
            args.exclude = '{}|{}'.format(args.exclude, EXCLUDE_PATTERN_AS_TEMPLATE)

    if args.exclude_git:
        if args.exclude is None:
            args.exclude = '^\.git/.*'
        else:
            args.exclude = '{}|^\.git/.*'.format(args.exclude)

    try:
        main(args)
    except RuntimeWarning as w:
        print(file=sys.stderr)
        print(w, file=sys.stderr)
        sys.exit(1)
    except (RuntimeError, KeyError) as e:
        print(file=sys.stderr)
        print('Error:', e, file=sys.stderr)
        if args.verbose:
            raise
        sys.exit(1)
    else:
        sys.exit(0)
